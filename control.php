<?php
/*
  PURPOSE: controller for epigram generator
    Composes output from one or more templates, selected according to scripted rules
  HISTORY:
    2017-12-29 started
*/

function DoTemplate($fn) {
    $fs = "data/$fn.edt";
    $oLoad = new cEpiLoader($fs);
    $oGen = $oLoad->GetGenerator();
    $out = $oGen->Generate();
    return $out;
}