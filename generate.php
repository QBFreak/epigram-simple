<?php
/*
  PURPOSE: epigram generator
*/
class cEpiLoader {
    public function __construct($fs) {
	$arLines = $this->LoadFile($fs);
	// LATER: add ability to split single data file into multiple blocks
	$oGen = new cEpiGenerator($arLines);
	$this->SetGenerator($oGen);
    }
    // RETURNS: array, one element per line (blank lines skipped)
    protected function LoadFile($fs) {
	if (file_exists($fs)) {
	    return file($fs,FILE_SKIP_EMPTY_LINES);
	} else {
	    die("FILE [$fs] not found.\n");
	}
    }

    // ++ PROPERTIES ++ //

    private $oGen;
    protected function SetGenerator(cEpiGenerator $oGen) {
	$this->oGen = $oGen;
    }
    public function GetGenerator() {
	return $this->oGen;
    }
}

class cEpiGenerator {

    /*----
      INPUT: array of lines to parse, one line per array element
      ACTION: Loads/parses text data needed to generate a single epigram
    */
    public function __construct(array $arBlock) {
	$this->ParseLines($arBlock);
    }


    // ++ DATA VALUES ++ //
    private $sMask;
    protected function SetMaskString($s) {
	$this->sMask = $s;
    }
    protected function GetMaskString() {
	return $this->sMask;
    }

    private $arWords=array(),$arIndex=NULL;
    protected function AddWord_byMask($sMask,$sWord) {
	$ar = str_split($sMask);	// convert string to array of single characters
	$pos = 0;
	foreach ($ar as $ch) {
	    if ($ch == '1') {
		$idx = count($this->arWords);
		$this->arWords[] = $sWord;
		$this->arIndex[$pos][] = $idx;
	    }
	    $pos++;
	}
    }
    protected function GetWord_forCol($col) {
	$arWIdxs = $this->arIndex[$col];
	$cntWords = count($arWIdxs);
	$nWord = rand(0,$cntWords-1);
	$idxWord = $arWIdxs[$nWord];
	$sWord = $this->arWords[$idxWord];
	return $sWord;
    }

    // -- DATA VALUES -- //
    // ++ INPUT CALCULATIONS ++ //

    protected function ParseLines(array $ar) {
	foreach ($ar as $sLine) {
	    // strip off comments
	    $pos = strpos($sLine,';');
	    if ($pos !== FALSE) {
		$sLine = substr($sLine,0,$pos);
	    }
	    $sLine = trim($sLine);	// strip whitespace from beginning and end
	    if ($sLine != '') {		// if there's still content...
		// split the line up
		$sArg = strpbrk($sLine," \t");

		$sFirst = substr($sLine,0,strlen($sLine)-strlen($sArg));
		$sArg = trim($sArg);

		switch ($sFirst) {
		  case '.PHRASE':
		    $this->SetMaskString($sArg);
		    break;
		  default:
		    // is binary selector - add arg to word list(s)
		    $this->AddWord_byMask($sFirst,$sArg);
		}
	    }
	}
    }

    // -- INPUT CALCULATIONS -- //
    // ++ OUTPUT CALCULATIONS ++ //

    /*----
      ACTION:
	For each '*' in the mask string, picks a random word for that slot from the word list
    */
    public function Generate() {
	$sMask = $this->GetMaskString();

	$arMask = preg_split('/\*/',$sMask);	// split wherever there's an asterisk
	$sOut = NULL;
	$nCol = 0;
	foreach ($arMask as $sChunk) {
	    if (is_null($sOut)) {
		$sOut = $sChunk;
	    } else {
		$sWord = $this->GetWord_forCol($nCol);
		$sOut .= $sWord.$sChunk;
		$nCol++;
	    }
	}
	return $sOut;
    }

    // -- OUTPUT CALCULATIONS -- //
}

function DoTemplate($fn) {
    $fp = dirname(__FILE__);
    $fs = "$fp/data/$fn.etd";
    $oLoad = new cEpiLoader($fs);
    $oGen = $oLoad->GetGenerator();
    $out = $oGen->Generate();
    return $out;
}
// TESTING
/*
$oLoad = new cEpiLoader('data/exclaim.etd');
$oGen = $oLoad->GetGenerator();
$out = $oGen->Generate();
echo "EPIGRAM: [$out]\n";
*/
include('scripts/exclaim.php');

echo $out."\n";
